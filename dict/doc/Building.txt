https://code.google.com/p/stardict-3/wiki/Building


How to build StarDict

    How to build StarDict
    Linux
        Ubuntu
        Fedora
    Mac OS X
    FreeBSD
    Windows
        Building StarDict
            GTK
            libxml library
                I. building with prebuild binaries
                II. building libxml library from source
            stardict-editor project configuration for libxml
            Building libsigc++
            sapi-tts plugin
            Acrobat wordpick plugin
            Notes
        Solution structure
        Unicode
        Build the installer
            Warning
            Note
            Deploying Visual C++ library with StarDict
            StarDict Portable installer
        Debugging
        Running StarDict on another computer
        StarDict version
        Two copies of CRT
        Notes

Linux

Build stardict-*.tar.xz

 ./autogen.sh
 ./configure --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man --disable-gucharmap --disable-gnome-support --disable-scrollkeeper
 make dist-xz
 

Build stardict-*.rpm

 cp dict/stardict.spec /root/rpmbuild/SPECS/
 cp stardict-*.tar.xz /root/rpmbuild/SOURCES/
 cd /root/rpmbuild/SPECS/
 rpmbuild -bb stardict.spec
 

Build stardict_*.deb

 cp -r dict/debian-upstream debian
 dpkg-buildpackage -rfakeroot


Create tags:
=====
git tag 2025.3.20
git push --tags origin
=====


To Install StarDict on Linux(or other Unix-like platform), type:

 ./configure --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man --disable-gucharmap --disable-gnome-support --disable-scrollkeeper
 make
 sudo make install
 

If compile festival plugin failed, you can add "--disable-festival" option to configure command.

You can use:

 ./configure --disable-gnome-support --disable-scrollkeeper --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man --disable-gucharmap
 

to build the "gtk library only" version of StarDict.

You can use

 ./configure --enable-gpe-support --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man
 

to build the GPE version of StarDict. For GPE, see http://gpe.handhelds.org

You can use

 ./configure --enable-maemo-support --prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man
 

to build the Maemo version of StarDict. For Maemo, see http://www.maemo.org

StarDict does not support staged installs, that is you may not use DESTDIR variable to install the application into a custom place like this:

 make DESTDIR=/custom/path install
 

StarDict needs to know at compile time all details of the installation otherwise it would not be able to find its data and it'd fail to start. If you need to change installation dirs use configure parameters like --prefix.

Notice: you can download dictionaries on the StarDict forum: http://www.stardict.org/forum/ or http://stardict.huzheng.org, http://download.huzheng.org


Ubuntu

Tested on Ubuntu 11.10

Assume we want to build development version of stardict, so the installation path will be $HOME/lab.

To build StarDict?, we need following packages installed.

 sudo apt-get install build-essential automake libtool libmysqlclient-dev libpcre3-dev
 sudo apt-get install gnome-common libgconf2-dev libenchant-dev libgucharmap2-dev libespeak-dev libgnomeui-dev festival-dev
 

Preparing Makefiles. Looks like we might need prepare /dict seperately.

 ./autogen.sh
 ./configure --prefix=$HOME/lab
 cd dict
 ./autogen.sh
 ./configure --prefix=$HOME/lab
 cd ..
 

Then we can build and install.

 make
 sudo make install
 

Fedora
Mac OS X

StarDict have been ported to Mac OS X now.

Here is a abstract introduction of compiling stardict and building the package.

    1. Install Xcode. 

    2. Install fink. 

http://www.finkproject.org This should be optional.

    3. Install jhbuild. 

http://developer.imendio.com/projects/gtk-macosx

http://developer.imendio.com/projects/gtk-macosx/build-instructions

And build gtk by following the instructions.

    4. Compile stardict. 

Edit configure.in, remove the "AM_GCONF_SOURCE_2" lines.

 ./autogen.sh
 export PKG_CONFIG=/opt/gtk/bin/pkg-config
 ./configure --prefix=/opt/gtk --disable-festival --disable-espeak --disable-gucharmap \
 --disable-spell --enable-darwin-support --mandir=/opt/gtk/share/man
 make
 sudo make install
 

    5. Create the install package. 

Read documents in http://gimp-app.sourceforge.net/

You can take the existing StarDict.pkg for the example.

To get the gtk library files, you can do:

 cd ~/Source/gnome/gtk+/;make DESTDIR=~/stardict-root install
 

    and so on. 

After Created the package directory, you can use PackageMaker to build it.

FreeBSD

Windows

See:
dict/msvc_2008/Building.txt
dict/msvc_2010/Building.txt


--------

Hu Zheng <huzheng_001@hotmail.com> http://www.huzheng.org

2024.1.4
